import sys

sys.path.insert(0, "/home/amper/AI/Harel/AI_Infrastructure/")  # For non-docker

from pathlib import Path
from typing import Union, List, Optional, Dict

import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
from torch.utils import data
from torch.utils.data import DataLoader
from torch.utils.data.dataloader import default_collate

from DataPipeline.src.GetEngines import get_engines
from OpenSource.WisenseEngine import WisenseEngine

UNCLASSIFIED = 'unclassified'


def get_instance_from_delta(delta_df, instance_data):
    mask = (delta_df['cluster_id'] == instance_data['cluster_id'])
    if instance_data['sub_cluster_id'] != -1 and not np.isnan(instance_data['sub_cluster_id']):
        if 'sub_cluster' not in delta_df:
            mask[:] = False
        else:
            mask &= delta_df['sub_cluster'] == instance_data['sub_cluster_id']
    delta_df = delta_df[mask]
    return delta_df


def load_delta_df(instance_data):
    radar_timestamp = instance_data['timestampRadar']
    delta_dir = Path(instance_data['dataset_dir']) / 'radar_data' / 'cuda_processed_delta'
    delta_df = pd.read_csv(delta_dir / f'cuda_processed_delta_{radar_timestamp}.csv')
    return delta_df


class SingleInstanceDataset(data.Dataset):
    def __init__(self, dataset_df, relevant_classes, features_to_use=(),
                 epoch_size=None, only_dynamic=False, max_velocity=None, balance=True):

        self.balance = balance
        self.features_to_use = ['x', 'y', 'z', *features_to_use]
        # self.features_mean = torch.zeros(len(features_to_use))
        # self.features_std = torch.ones(len(features_to_use))
        # for i, feature in features_to_use:
        #     if feature in features_normalizations.keys():
        #         pass
        #         # self.features_mean =.............

        self.only_dynamic = only_dynamic
        self.max_velocity = max_velocity
        drives_names = dataset_df['drive_name'].unique()
        self.engines: Dict[str, WisenseEngine] = {
            drives_name: get_engines(wf="92", drive_str=drives_name)[0]
            for drives_name in drives_names
        }
        for engine in self.engines.values():
            engine.drive_context.cuda_processed_delta = engine.drive_context.cuda_processed_delta.replace(
                'workspace', 'mnt').replace('data_backup7', 'nvme1')
        self.is_train = all(['train' in engine.suffix.lower() for engine in self.engines.values()])



        dataset_df.loc[dataset_df['cat'].isna(), 'cat'] = UNCLASSIFIED
        dataset_df.loc[dataset_df['cat_original'].isna(), 'cat_original'] = UNCLASSIFIED

        if not self.is_train or True:
            high_certainty_mask = (dataset_df['cat_original'] != UNCLASSIFIED) | (dataset_df['cat'] == UNCLASSIFIED)
            dataset_df = dataset_df[high_certainty_mask]
            has_association_mask = (dataset_df['cat_original'] != UNCLASSIFIED)
            dataset_df.loc[has_association_mask, 'cat'] = dataset_df.loc[has_association_mask, 'cat_original']

        for idx, cls in enumerate(relevant_classes):
            if isinstance(cls, str):
                relevant_classes[idx] = [cls]
        relevant_classes_flatten = [cls for cls_list in relevant_classes for cls in cls_list]
        relevant_classes_mask = dataset_df['cat'].isin(relevant_classes_flatten)
        unclassified_mask = np.logical_not(relevant_classes_mask)

        dataset_df['classification_label'] = dataset_df['cat']

        # dataset_df['classification_label'].loc[unclassified_mask] = UNCLASSIFIED

        dataset_df = dataset_df[relevant_classes_mask]  # TODO

        if only_dynamic:
            dataset_df = dataset_df[dataset_df['vel'] > 0]
        if max_velocity is not None:
            dataset_df = dataset_df[dataset_df['vel'] < max_velocity]

        # dataset_df = dataset_df[dataset_df['r'] < 50]
        # dataset_df = dataset_df[np.abs(dataset_df['x']) <= 20]

        self.df: pd.DataFrame = dataset_df

        if epoch_size is None:
            epoch_size = len(self.df)
        self.epoch_size = epoch_size

        relevant_classes = [*list(sorted(relevant_classes))]

        for idx, cls in enumerate(relevant_classes):
            joined_classes_mask = self.df['classification_label'].isin(cls)
            joined_class_name = ' or '.join(cls)
            self.df['classification_label'].loc[joined_classes_mask] = joined_class_name
            relevant_classes[idx] = joined_class_name

        self.class_to_idx_dict = {cls: idx for idx, cls in enumerate(relevant_classes)}
        self.class_to_df_dict = {cat: cat_df for cat, cat_df in self.df.groupby('classification_label')}
        self.class_to_count_dict = {cat: len(cat_df) for cat, cat_df in self.class_to_df_dict.items()}
        print(self.class_to_idx_dict)
        self.classes_list = relevant_classes
        print(self.df.groupby('classification_label').count()['idx'])

    @property
    def num_of_classes(self):
        return len(self.class_to_idx_dict)

    def __len__(self):
        return int(self.epoch_size)

    def prepare_sample(self, instance_data):
        engine = self.engines[instance_data['drive_name']]
        idx = instance_data['idx']
        delta_df = pd.read_csv(engine.get_processed_delta(idx, path_only=True))
        instance_df = get_instance_from_delta(delta_df, instance_data)
        instance_df['num_of_points'] = instance_df.shape[0]
        if len(instance_df) == 0:
            return None

        instance_df = instance_df[self.features_to_use]
        point_cloud = instance_df.to_numpy()
        if self.is_train:
            augmentation_scale = instance_df.std() / 2
            augmentation_scale[['x', 'y', 'z', 'r']] = 0.1
            augmentation_shift = np.random.randn(*instance_df.shape) * augmentation_scale.to_numpy()
            point_cloud += augmentation_shift

            # random drop
            keep_mask = np.random.rand(len(point_cloud)) < 0.9
            if np.count_nonzero(keep_mask) < 2:
                keep_mask[::2] = True
            point_cloud = point_cloud[keep_mask]

        point_cloud = torch.from_numpy(point_cloud).float().T

        label = torch.full((1,), self.class_to_idx_dict[instance_data['classification_label']])

        metadata = instance_data.to_dict()
        metadata['label_index'] = label

        return {
            'cluster_id': instance_data['cluster_id'],
            'sub_cluster_id': instance_data['sub_cluster_id'],
            'model_input': point_cloud,
            'label': label,
            'label_name': instance_data['classification_label'],
            'label_original_name': instance_data['cat'],
            'metadata': metadata
        }

    def __getitem__(self, idx):
        if self.balance:
            class_to_sample = self.classes_list[idx % self.num_of_classes]
            instance_data = self.class_to_df_dict[class_to_sample].sample().iloc[0]
        else:
            instance_data = self.df.iloc[idx]

        while True:
            output = self.prepare_sample(instance_data)
            if output is None:  # Bad sample
                # Sample from the same category
                instance_data = self.class_to_df_dict[instance_data['classification_label']].sample().iloc[0]
                continue
            else:
                return output


def collate_point_clouds(batch):
    if len(batch) > 1:
        lens = np.array([item['model_input'].shape[1] for item in batch])

        max_len = max(lens)
        copies_needed = np.ceil(max_len / lens).astype(int)
        for copies, item in zip(copies_needed, batch):
            item['model_input'] = item['model_input'].repeat(1, copies)[:, :max_len]

    return default_collate(batch)


class ClassificationDataModule(pl.LightningDataModule):
    def __init__(self, classes_to_keep, features_to_use=(), only_dynamic=False, max_velocity=None, batch_size=1,
                 epoch_size=None):
        super().__init__()
        self.max_velocity = max_velocity
        self.only_dynamic = only_dynamic
        self.batch_size = batch_size
        self.epoch_size = epoch_size
        self.features_to_use = features_to_use
        self.classes_to_keep = classes_to_keep

    def setup(self, stage: Optional[str] = None):
        base_path = Path('/home/amper/AI/Meir/radar_perception/dataset_analysis/instances_and_tracks/')
        train_df = pd.read_pickle(base_path / 'classification_train_dataset.p')
        val_df = pd.read_pickle(base_path / 'classification_val_dataset.p')

        self.train_dataset = SingleInstanceDataset(
            train_df, self.classes_to_keep, self.features_to_use, self.epoch_size, self.only_dynamic, self.max_velocity
        )

        val_size = self.epoch_size / 10 if self.epoch_size is not None else None  # TODO: Something better and less random
        self.val_dataset = SingleInstanceDataset(
            val_df, self.classes_to_keep, self.features_to_use, val_size, self.only_dynamic, self.max_velocity
        )

    def train_dataloader(self):
        return DataLoader(self.train_dataset, num_workers=8,
                          batch_size=self.batch_size, shuffle=True, drop_last=True, collate_fn=collate_point_clouds)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, num_workers=8,
                          batch_size=self.batch_size, shuffle=False, drop_last=True, collate_fn=collate_point_clouds)

# class FullSceneDataset(data.Dataset):
#     def __init__(self, dataset_dir: Path, features_to_use=()):
#         self.features_to_use = ('x', 'y', 'z', *features_to_use)
#         self.delta_dir = dataset_dir / 'radar_data' / 'cuda_processed_delta'
#         self.lidar_labels_dir = dataset_dir / 'lidar_labels' / 'camera_to_lidar'
#
#         sync_path = dataset_dir / 'aux' / 'sync_data_radar_lidar.csv'
#         self.df: pd.DataFrame = pd.read_csv(sync_path)
#
#     def load_labels(self, frame_data):
#         lidar_timestamp = frame_data['timestampLidar']
#         with open(self.lidar_labels_dir / f'camera_to_lidar_{lidar_timestamp}.json', 'r') as f:
#             labels = json.load(f)
#             labels = [lab for lab in labels if '3d_center_clustering' in lab]
#         return labels
#
#     def extract_centers_of_mass_label(self, raw_labels):
#         pass
#
#     def load_delta_cloud(self, frame_data):
#         radar_timestamp = frame_data['timestampRadar']
#         delta_df = pd.read_csv(self.delta_dir / f'cuda_processed_delta_{radar_timestamp}.csv')
#         delta_df = delta_df[self.features_to_use]
#         return delta_df
#
#     def __len__(self):
#         return len(self.df)
#
#     def __getitem__(self, idx):
#         frame_data: pd.Series = self.df.iloc[idx]
#         point_cloud_df = self.load_delta_cloud(frame_data)
#         point_cloud = torch.from_numpy(point_cloud_df.values).float()
#
#         # label = torch.from_numpy(np.array([self.class_to_num_dict[sample['type']]]))
#         label = torch.full((1,), self.class_to_num_dict[sample['type']])
#         # print('getitem', objects_point_cloud.shape, label.shape)
#
#         if objects_point_cloud.shape[1] < 200:
#             # idx = random.randint(0, len(self.df))
#             sample = self.df_per_class[sample['type']].sample().iloc[0]
#             continue
#
#         # Build sample
#         item = {'model_input': objects_point_cloud,
#                 'label': label,
#                 'metadata': sample.to_dict()
#                 }
#
#         return item
