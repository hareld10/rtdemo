from pathlib import Path

import cv2
import numpy as np
import pandas as pd

"""
Frame Data:
['idRadar',
 'timestampRadar',
 'filenameRadar',
 'inFileIdxRadar',
 'timeInt',
 'timestampLidar',
 'idCamera',
 'timestampCamera',
 'filenameCamera',
 'inFileIdxCamera',
 'dataset_dir']
"""

HDD_BASE_PATH = '/workspace/data_backup7'
SSD_BASE_PATH = '/mnt/nvme1'


def ssd(s: str):
    return s.replace(HDD_BASE_PATH, SSD_BASE_PATH)


def load_delta_cloud(frame_data):
    radar_timestamp = frame_data['timestampRadar']
    delta_dir = Path(ssd(frame_data['dataset_dir'])) / 'radar_data' / 'cuda_processed_delta'
    delta_df = pd.read_csv(delta_dir / f'cuda_processed_delta_{radar_timestamp}.csv')
    delta_df["Az"] = np.rad2deg(np.arcsin(delta_df["u_sin"]))
    delta_df["El"] = np.rad2deg(np.arcsin(delta_df["v_sin"]))
    return delta_df


def load_lidar_cloud(frame_data, as_dataframe=False):
    lidar_timestamp = str(frame_data['timestampLidar'])[:-4]
    lidar_pc_dir = Path(frame_data['dataset_dir']) / 'lidar_data' / 'lidar_pc'
    lidar_pc_path = lidar_pc_dir / f'lidar_pc_{lidar_timestamp}.npz'
    lidar_pc = np.load(lidar_pc_path)['arr_0']
    if as_dataframe:
        return pd.DataFrame(lidar_pc, columns=['x', 'y', 'z'])
    else:
        return lidar_pc


def load_image(frame_data):
    im_path = Path(frame_data.dataset_dir) / 'camera_data' / 'camera_rgb_rectified' / \
              f'camera_rgb_{frame_data["timestampCamera"]}.png'
    im = cv2.imread(str(im_path))[..., ::-1]
    return im
