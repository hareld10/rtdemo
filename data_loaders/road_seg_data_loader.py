import warnings
from collections import defaultdict

import numpy as np
import pandas as pd
import scipy.ndimage as ndi
import torch
from shapely.geometry import Point
from torch.utils.data.dataloader import default_collate

warnings.filterwarnings("ignore")


def to_bins(vec, bins):
    vec = vec + (bins[1] - bins[0]) / 2
    return np.digitize(vec, bins) - 1


def generate_bins(voxels_params):
    return {key: np.arange(val['range'][0], val['range'][1] + val['range'][2] * 0.1, val['range'][2])
            for key, val in voxels_params.items()}


optional_tasks = ['freespace', 'vehicle', 'pedestrian']


class RoadSegmentationDataset():
    def __init__(self, val_train, task, epoch_size, features, voxels_params, label_params, time_concat,
                 num_points_set_abstraction, cartesian_loss, radar_type, loss_range_scaling_factor=0.5, **kwargs):
        assert task in optional_tasks
        print(f'======================= Task: {task} =======================')
        if (len(kwargs) > 0):
            print(kwargs)
            raise Exception()
        global RADAR_TYPE_GLOBAL
        RADAR_TYPE_GLOBAL = radar_type
        if radar_type == 'pcp':
            assert time_concat == 0

        self.loss_range_scaling_factor = loss_range_scaling_factor

        self.val_train = val_train
        self.task = task
        self.radar_type = radar_type
        self.num_points_set_abstraction = num_points_set_abstraction
        self.time_concat = time_concat
        self.label_params = label_params
        if not isinstance(features, dict):
            assert isinstance(features, list)
            features = {'all': features}
        self.features = features
        self.voxels_params = voxels_params
        self.grid_bins = generate_bins(voxels_params)
        self.epoch_size = epoch_size

        r_bins = self.grid_bins['r']
        az_bins = self.grid_bins['Az']
        self.grid_shape = (len(r_bins), len(az_bins))
        self.shapely_grid = np.zeros(self.grid_shape, dtype=np.object_)

        grid_df_xy = []
        for i_r, val_r in enumerate(r_bins):
            for i_az, val_az in enumerate(az_bins):
                _x = val_r * np.sin(np.deg2rad(val_az))
                _y = val_r * np.cos(np.deg2rad(val_az))
                self.shapely_grid[i_r, i_az] = Point(_x, _y)
                grid_df_xy.append({
                    'i_r': i_r, 'i_Az': i_az, 'x': _x, 'y': _y, 'r': val_r, 'Az': val_az
                })
        self.grid_df_xy = pd.DataFrame(grid_df_xy)

        self.loss_weights = self.generate_cartesian_loss_grid()
        if not cartesian_loss:
            self.loss_weights[:] = 1

    def __len__(self):
        return int(self.epoch_size)

    def generate_cartesian_loss_grid(self):
        loss_weight_grid = self.gen_empty_grid(dtype=float)
        r_bins = self.grid_bins['r']
        az_bins = self.grid_bins['Az']

        az_bin_size = np.deg2rad(az_bins[1] - az_bins[0])

        for i_r, r in enumerate(r_bins):
            loss_weight_grid[i_r, :] = .5 * az_bin_size * ((r + az_bin_size / 2) ** 2 - (r - az_bin_size / 2) ** 2)

        if self.loss_range_scaling_factor > 0:
            loss_weight_grid = loss_weight_grid * (
                    1 + r_bins.reshape(-1, 1) / r_bins.max() * self.loss_range_scaling_factor)

        loss_weight_grid = loss_weight_grid / loss_weight_grid.sum() * loss_weight_grid.size  # Normalize for equal sum
        # loss_weight_grid = loss_weight_grid / loss_weight_grid.min()
        return loss_weight_grid

    def add_indices_to_cloud(self, point_cloud_df):
        for var in self.grid_bins.keys():
            point_cloud_df[var + '_indices'] = to_bins(point_cloud_df[var], self.grid_bins[var])

        bins_cols = [var + '_indices' for var in self.grid_bins.keys()]
        # point_cloud_df = point_cloud_df.sample(frac=1).reset_index(drop=True)
        # point_cloud_df['index_col'] = point_cloud_df.index

        return point_cloud_df, bins_cols

    def filter_cloud(self, point_cloud_df):
        mask = np.logical_and.reduce([
            (point_cloud_df[var] >= self.voxels_params[var]['range'][0]) &
            (point_cloud_df[var] <= self.voxels_params[var]['range'][1])
            for var, spec in self.voxels_params.items() if spec['filter_outside']
        ])
        point_cloud_df = point_cloud_df[mask]
        return point_cloud_df

    def gen_empty_grid(self, dtype=int):
        label_shape = [len(self.grid_bins[k]) for k in self.label_params['projection']]
        bev_vehicle_grid = np.zeros(label_shape, dtype=dtype)
        return bev_vehicle_grid

    def bev_df_to_label(self, bev_df):
        bev_df = self.filter_cloud(bev_df)
        for var in self.label_params['projection']:
            bev_df[var + '_indices'] = to_bins(bev_df[var], self.grid_bins[var])

        bins_cols = [var + '_indices' for var in self.label_params['projection']]
        stats = bev_df.groupby(bins_cols, as_index=False)['z'].first()
        indices = tuple(stats[bins_cols].values.T.tolist())

        bev_occupancy_grid = self.gen_empty_grid()
        bev_occupancy_grid[indices] = 1
        bev_occupancy_grid = ndi.minimum_filter(ndi.maximum_filter(bev_occupancy_grid, (3, 5)), (3, 5))
        bev_occupancy_grid = ndi.maximum_filter(ndi.minimum_filter(bev_occupancy_grid, (3, 3)), (3, 3))

        return bev_occupancy_grid

    def delta_to_set_abstraction(self, delta_df_all):
        delta_df_all, bins_cols = self.add_indices_to_cloud(delta_df_all)
        split_abstraction_dict = {}
        for split, features in self.features.items():
            if split == 'all':
                delta_df_split = delta_df_all
            elif split == 'dynamic':
                delta_df_split = delta_df_all[delta_df_all.trk_id > -1]
            elif split == 'static':
                delta_df_split = delta_df_all[delta_df_all.trk_id == -1]
            else:
                raise KeyError(f'{split} is not a valid split of the radar point cloud')

            groupby = delta_df_split.groupby(bins_cols, as_index=True)
            counts = groupby['x'].count()
            voxels_below = counts.index[counts <= self.num_points_set_abstraction]
            voxels_above = counts.index[counts > self.num_points_set_abstraction]

            tuples_in_df = pd.MultiIndex.from_frame(delta_df_split[bins_cols])
            num_above = len(voxels_above)
            num_below = len(voxels_below)

            grouped_points = np.zeros((num_above + num_below, self.num_points_set_abstraction, len(features)))

            if num_above > 0:
                delta_above = delta_df_split[tuples_in_df.isin(voxels_above)].sort_values(
                    'FFT_Power', ascending=False).groupby(bins_cols).head(self.num_points_set_abstraction)
                grouped_points[-num_above:, ...] = delta_above[features].values.reshape(num_above,
                                                                                        self.num_points_set_abstraction,
                                                                                        len(features))
            if num_below > 0:
                delta_below = delta_df_split[tuples_in_df.isin(voxels_below)]
                delta_below = delta_below.sort_values(bins_cols)
                below_in_voxel_index = np.concatenate(counts[voxels_below].apply(np.arange).values)
                below_sample_ind = np.repeat(np.arange(num_below), counts[voxels_below])

                delta_below_features = delta_below[features].to_numpy()

                grouped_points[:num_below, :] = delta_below_features[below_in_voxel_index == 0].reshape(-1, 1,
                                                                                                        len(features))
                grouped_points[below_sample_ind, below_in_voxel_index, :] = delta_below_features

            voxel_grid_indices_list = np.array(voxels_below.to_list() + voxels_above.to_list())
            assert bins_cols[0] == 'r_indices'
            if voxel_grid_indices_list.shape[0] == 0:
                split_abstraction_dict[split] = {
                    'grouped_points': grouped_points.reshape(0, self.num_points_set_abstraction, len(features)),
                    'voxel_grid_indices_list': voxel_grid_indices_list.reshape(0, 2)}
                continue
            az_all = np.deg2rad(self.grid_bins['Az'][voxel_grid_indices_list[:, 1]])
            r_all = self.grid_bins['r'][voxel_grid_indices_list[:, 0]]
            if features[0] == 'r':
                assert features[0] == 'r'
                assert features[1] == 'Az'
                grouped_points[:, :, 0] -= r_all.reshape(-1, 1)
                grouped_points[:, :, 1] -= az_all.reshape(-1, 1)
            elif features[0] == 'x':
                assert features[0] == 'x'
                assert features[1] == 'y'
                x_all = r_all * np.sin(az_all)
                y_all = r_all * np.cos(az_all)
                grouped_points[:, :, 0] -= x_all.reshape(-1, 1)
                grouped_points[:, :, 1] -= y_all.reshape(-1, 1)
            split_abstraction_dict[split] = {'grouped_points': grouped_points,
                                             'voxel_grid_indices_list': voxel_grid_indices_list}
        return split_abstraction_dict

    def delta_to_model_input(self, delta_df):
        if delta_df is None:
            return None
        split_abstraction_dict = self.delta_to_set_abstraction(delta_df)
        if split_abstraction_dict is None:
            print('Bad Input')
            return None
        model_input = {
            split: {
                'grouped_points': torch.from_numpy(s_dict['grouped_points']).float(),
                'voxel_indices': torch.from_numpy(s_dict['voxel_grid_indices_list']),
                'num_of_groups': s_dict['grouped_points'].shape[0],
                'grid_shape': np.array(self.grid_shape)
            }
            for split, s_dict in split_abstraction_dict.items()
        }
        return model_input


def collate_grouped_point_clouds(batch):
    batch = [x for x in batch if x is not None]
    if len(batch) > 1:
        batch_output = dict()
        batch_output['model_input'] = defaultdict(dict)
        for split in batch[0]['model_input'].keys():
            for key in batch[0]['model_input'][split].keys():
                if key in ['grouped_points', 'voxel_indices']:
                    batch_output['model_input'][split][key] = torch.vstack(
                        [item['model_input'][split][key] for item in batch if split in item['model_input']])
                else:
                    batch_output['model_input'][split][key] = default_collate(
                        [item['model_input'][split][key] for item in batch if split in item['model_input']])

        for key in batch[0].keys():
            if key != 'model_input':
                batch_output[key] = default_collate([item[key] for item in batch])
    else:
        batch_output = default_collate(batch)
    return batch_output
