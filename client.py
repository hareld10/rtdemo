import concurrent.futures
import pickle
import time
# from multiprocessing import Queue
from queue import LifoQueue
from queue import Queue
from PyQt5 import QtCore

import cv2
import requests

MAX_WORKERS = 2


class Manager(QtCore.QRunnable):
    def __init__(self):
        super(Manager, self).__init__()
        self.q_in = Queue(maxsize=1)
        self.q_out = Queue(maxsize=100)
        self.timestamps = LifoQueue(maxsize=100)
        self.out_dict = {}
        self.ports = [2402]
        self.data = None
        self.execuator = concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS)

    def process_request(self, server, pcp):
        # to run multithred, put these function in other thread 
        cur_server = self.ports[server % len(self.ports)]
        ret = requests.post(f'http://0.0.0.0:{cur_server}/pcp', data=pickle.dumps(pcp))
        prediction = pickle.loads(ret.content)
        # print("process_request: got from service", type(prediction))
        try:
            # print("process_request: putting to q_out")
            self.q_out.put(prediction, block=False)
        except Exception as e:
            pass
            # print("process_request: exception", e)

    @QtCore.pyqtSlot()
    def run(self):
        server = 0
        while True:
            try:
                packet = self.q_in.get(block=False)
                # print("packet.shape", type(packet))
            except:
                time.sleep(0.001)
                continue

            # pcp, header, meta_data = packet

            self.execuator.submit(self.process_request, server, packet)

            s_time = time.time()

            server += 1
