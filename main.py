import json
import sys
from pathlib import Path

sys.path.insert(0, "/home/wisense/workspace/wise_sdk/SDK/python")  # For non-docker
import WiseSDK

import argparse
import pickle
import time
import traceback


def pickle_read(file_path):
    """
    De-serialize an object from a provided file_path
    """

    with open(file_path, 'rb') as file:
        return pickle.load(file)


import pandas as pd
from flask import Flask, request

from model.model_api import gen_predictions

app = Flask(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("--port", type=int, default=2402)
parser.add_argument("--gpu", type=str, default='0')
args = parser.parse_args()

saved_delta_path = Path('/home/wisense/AI/delta')


@app.route('/pcp', methods=['POST'])
def pcp():
    if request.method == 'POST':
        s_time = time.time()
        req_data = request.get_data()
        try:
            # pcp, header, metadata, ai_params = pickle.loads(req_data)
            pcp, header, metadata = pickle.loads(req_data)
        except ModuleNotFoundError:
            print(traceback.format_exc())
            return {}
        with open('ai_config.json') as json_file:
            ai_config = json.load(json_file)

        pred = {}
        if any(list(ai_config['flags'].keys())):
            pcp_input = pcp.copy()
            pcp_dict = {'data': pcp_input, 'metadata': metadata, 'header': header}
            pred = gen_predictions(pcp_dict, ai_config)

            max_range = ai_config['vehicle_config']['max_range']
            # pcp = pcp[pcp.r < max_range]

        ret_packet = pickle.dumps({"pcp": pcp, "header": header, "metadata": metadata, "pred": pred})
        print(pred.keys())
        print("service time", time.time() - s_time)
        return ret_packet


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=args.port, debug=False, threaded=False)
