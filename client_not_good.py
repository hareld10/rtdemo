import concurrent.futures
import os
import time
from multiprocessing import Queue
import pickle
import cv2
import matplotlib.pyplot as plt
import numpy as np
import requests
from queue import PriorityQueue, LifoQueue


class Manager:
    def __init__(self):
        self.q_in = LifoQueue(maxsize=4)
        self.q_out = Queue()
        self.timestamps = Queue(maxsize=2)
        self.out_dict = {}
        self.execuator = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.sender = concurrent.futures.ThreadPoolExecutor(max_workers=4)
        self.execuator.submit(self.run)
        self.execuator.submit(self.output)
        self.ports = [2403, 2404]
        self.data, self.ts = None, None
        self.prediction = None
        self.timestamp_out = None

        # self.ports = [2451, 2452, 2453]
        # self.ports = [2451]
        # s_port = 2425
        # n_ports = 8
        # self.ports = list(range(s_port, s_port + n_ports))

    def process_request(self, server, data, timestamp):
        cur_server = self.ports[server % len(self.ports)]
        s_time = time.time()
        packet = {"data": data, "ts_radar": time.time()}
        ret = requests.post(f'http://0.0.0.0:{cur_server}/pcp', data=pickle.dumps(packet))
        prediction = pickle.loads(ret.content)
        self.prediction = prediction
        self.timestamp_out = timestamp
        print("cur_server", cur_server, time.time() - s_time)

    def run(self):
        server = 0
        print("in run")
        while True:
            if self.data is None:
                print("self.data = None", self.data)
                time.sleep(0.05)
                continue
            self.sender.submit(self.process_request, server, self.data, self.ts)
            server += 1

    def output_timestamps(self):
        start_time = time.time()
        x = 1  # displays the frame rate every 1 second
        counter = 0

        while True:
            if self.timestamps.empty():
                time.sleep(0.01)
                continue
            desired_next = self.timestamps.get(block=False)
            while True:
                if desired_next in self.out_dict:
                    prediction = self.out_dict[desired_next]
                    counter += 1
                    if (time.time() - start_time) > x:
                        print("FPS: ", counter / (time.time() - start_time))
                        counter = 0
                        start_time = time.time()

                    self.out_dict.pop(desired_next)
                    if len(prediction) == 0:
                        print("len(prediction)==0")
                        break
                    pred = prediction["res"]
                    cv2.imshow('frame', pred.T)
                    cv2.waitKey(1)
                    break
                time.sleep(0.01)

    def output(self):
        output_time = time.time()
        while True:
            if self.prediction is None:
                time.sleep(0.01)
                continue
            prediction = self.prediction
            timestamp = self.timestamp_out
            # print(prediction)
            print(timestamp, "FPS: ", 1.0 / (time.time() - output_time))
            if len(prediction) == 0:
                print("len(prediction)==0")
                break
            pred = prediction["res"]
            cv2.imshow('frame', pred.T)
            cv2.waitKey(1)
            output_time = time.time()

    def set(self, timestamp, data):
        self.data = data
        self.ts = timestamp
        # if self.q_in.full() or self.timestamps.full():
        #     self.q_in.get()
        #     self.timestamps.get()
        # self.timestamps.put(timestamp, block=False)
        # self.q_in.put((timestamp, data), block=False)


m = Manager()
while True:
    for i in range(0, 1000):
        m.set(timestamp=str(time.time()), data=i)
        time.sleep(0.01)
    break
print("finished")
