import json
import requests
import numpy as np
import pandas as pd
from flask import Flask, jsonify, request
from tqdm import tqdm
import time
app = Flask(__name__)

arr = np.random.random((400, 3))
df = pd.DataFrame(arr)

packet = {"pcp": df.to_json(), "ts_radar": time.time()}
ret = requests.post(f'http://0.0.0.0:2401/pcp', json=packet)

