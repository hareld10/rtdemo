import torch.nn as nn
import torch.nn.functional as F
from pointnet.pointnet_utils import PointNetEncoder


class PointNet(nn.Module):
    def __init__(self, num_class, num_of_features=0):
        super(PointNet, self).__init__()
        self.feat = PointNetEncoder(global_feat=True, feature_transform=True, channel=3+num_of_features)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, num_class)
        self.dropout = nn.Dropout(p=0.4)
        self.bn1 = nn.Identity()  # nn.BatchNorm1d(512)
        # self.bn1 = nn.InstanceNorm1d(512)

        self.bn2 = nn.Identity()  # nn.BatchNorm1d(256)
        # self.bn2 = nn.InstanceNorm1d(256)  # nn.BatchNorm1d(256)
        self.relu = nn.ReLU()

    def forward(self, x):
        x, trans, trans_feat = self.feat(x)
        x = F.relu(self.bn1(self.fc1(x)))
        x = F.relu(self.bn2(self.dropout(self.fc2(x))))
        x = self.fc3(x)
        x = F.log_softmax(x, dim=1)
        return {'prediction': x, 'trans_feat': trans_feat}

# class get_loss(torch.nn.Module):
#     def __init__(self, mat_diff_loss_scale=0.001):
#         super(get_loss, self).__init__()
#         self.mat_diff_loss_scale = mat_diff_loss_scale
#
#     def forward(self, pred, target, trans_feat):
#         loss = F.nll_loss(pred, target)
#         mat_diff_loss = feature_transform_reguliarzer(trans_feat)
#
#         total_loss = loss + mat_diff_loss * self.mat_diff_loss_scale
#         return total_loss
