import subprocess
import time
import os
import pathlib
module_path = str(pathlib.Path(__file__).parent.absolute())
sleep = 2
p3 = "/home/amper/AI/Harel/AI_Infrastructure/Calibrator/venv/bin/python3"
s_port = 2425
n_ports = 8
ports = list(range(s_port, s_port+n_ports))

for port in ports:
    subprocess.Popen(f"{p3} {module_path}/main.py --port {port}", shell=True)
    time.sleep(sleep)

# subprocess.Popen(f"{p3} {module_path}/main.py --port 2403", shell=True)
# time.sleep(sleep)