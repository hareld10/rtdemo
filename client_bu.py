import concurrent.futures
import os
import time
from multiprocessing import Queue
import pickle
import cv2
import matplotlib.pyplot as plt
import numpy as np
import requests
from queue import PriorityQueue, LifoQueue


class Manager:
    def __init__(self):
        self.q_in = LifoQueue(maxsize=100)
        self.q_out = Queue(maxsize=100)
        self.timestamps = LifoQueue(maxsize=100)
        self.out_dict = {}
        self.execuator = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        self.sender = concurrent.futures.ThreadPoolExecutor(max_workers=4)
        self.execuator.submit(self.run)
        self.execuator.submit(self.output)
        self.ports = [2403, 2404]
        self.data = None
        # self.ports = [2451, 2452, 2453]
        # self.ports = [2451]
        # s_port = 2425
        # n_ports = 8
        # self.ports = list(range(s_port, s_port + n_ports))

    def process_request(self, server, data, timestamp):
        cur_server = self.ports[server % len(self.ports)]
        s_time = time.time()
        packet = {"data": data, "ts_radar": time.time()}
        ret = requests.post(f'http://0.0.0.0:{cur_server}/pcp', data=pickle.dumps(packet))
        prediction = pickle.loads(ret.content)
        # self.out_dict[timestamp] = prediction
        self.q_out.put(prediction, block=False)
        print("cur_server", cur_server, time.time() - s_time)

    def run(self):
        server = 0
        while True:
            if self.q_in.empty():
                time.sleep(0.02)
                continue

            timestamp, data = self.q_in.get()
            # print("Insides", data)
            with self.q_in.mutex:
                with self.timestamps.mutex:
                    self.q_in.queue.clear()
                    self.timestamps.queue.clear()
            self.sender.submit(self.process_request, server, data, timestamp)
            server += 1

    def output_timestamps(self):
        start_time = time.time()
        x = 1  # displays the frame rate every 1 second
        counter = 0

        while True:
            if self.timestamps.empty():
                time.sleep(0.01)
                continue
            desired_next = self.timestamps.get(block=False)
            while True:
                if desired_next in self.out_dict:
                    prediction = self.out_dict[desired_next]
                    counter += 1
                    if (time.time() - start_time) > x:
                        print("FPS: ", counter / (time.time() - start_time))
                        counter = 0
                        start_time = time.time()

                    self.out_dict.pop(desired_next)
                    if len(prediction) == 0:
                        print("len(prediction)==0")
                        break
                    pred = prediction["res"]
                    cv2.imshow('frame', pred.T)
                    cv2.waitKey(1)
                    break
                time.sleep(0.01)

    def output(self):
        output_time = time.time()
        print("in output")
        while True:
            print("self.q_out.size", self.q_out.qsize())
            if self.q_out.empty():
                time.sleep(0.2)
                continue

            timestamp, prediction = self.q_out.get(block=False)

            # print(prediction)
            print(timestamp, "FPS: ", 1.0 / (time.time() - output_time))
            if len(prediction) == 0:
                print("len(prediction)==0")
                break
            pred = prediction["res"]
            cv2.imshow('frame', pred.T)
            cv2.waitKey(1)
            output_time = time.time()
            print("Finish loop")

    def output_old(self):
        output_time = time.time()
        while True:
            if self.q_out.empty():
                time.sleep(0.01)
                continue
            timestamp, prediction = self.q_out.get()


            # print(prediction)
            print(timestamp, "FPS: ", 1.0 / (time.time() - output_time))
            if len(prediction) == 0:
                print("len(prediction)==0")
                break
            pred = prediction["res"]
            cv2.imshow('frame', pred.T)
            cv2.waitKey(1)
            output_time = time.time()

    def set(self, timestamp, data):
        # if self.q_in.full() or self.timestamps.full():
        #     self.q_in.get()
        #     self.timestamps.get()
        self.timestamps.put(timestamp, block=False)
        self.q_in.put((timestamp, data), block=False)


m = Manager()
while True:
    for i in range(0, 1000):
        m.set(timestamp=str(time.time()), data=i)
        time.sleep(0.1)
    break
print("finished")
