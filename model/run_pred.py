import sys
import numpy as np
import pandas as pd
from pathlib import Path
import os
import yaml
# import seaborn as sns
from lightning_module_segmentation import BevRoadSegNet
from multiprocessing import Pool
import torch
import cv2
# import seaborn as sns
from tqdm import tqdm
import warnings

warnings.filterwarnings("ignore")
from datetime import datetime

base_path = Path("/mnt/HDD/Meir/radar/radar_road_seg")
from data_loaders.road_seg_data_loader import RoadSegmentationDataset

# experiment = 'only_vehicle_hard_label_first_try/21-12-07-12:28:06'
# experiment = 'multiclass_hard_label_first_try/21-12-07-15:33:11'
# experiment = 'vehicle_soft_label_multiframe_input/21-12-13-22:48:43'
# experiment = 'multiclass_hard_label_multiframe_input/21-12-12-23:38:04'
# experiment = 'vehicle_soft_label_multiframe_input/21-12-14-08:56:02'
experiment = 'vehicle_soft_label_multiframe_input/21-12-15-22:23:50'

cp_dir = base_path / 'checkpoints' / experiment
checkpoints_sorted = sorted(cp_dir.iterdir(), key=os.path.getmtime)
best_cp_path = checkpoints_sorted[-1]
# best_cp_path = checkpoints_sorted[-1]#max(cp_dir.iterdir(), key=os.path.getmtime)  # cp_dir / sorted(os.listdir(cp_dir))[-1]
epoch_num = int(best_cp_path.name.lstrip(experiment.split('/')[0] + '-epoch=').split('-')[0])
# best_cp_path = cp_dir / [cp for cp in os.listdir(cp_dir) if cp.startswith('best')][0]
model = BevRoadSegNet.load_from_checkpoint(best_cp_path).double()
print(best_cp_path)
print(datetime.fromtimestamp(os.path.getctime(best_cp_path)))

config_path = base_path / 'logs' / experiment / 'config.yaml'
# config_path = '../config.yaml'
with open(config_path, 'r') as f:
    config = yaml.load(f, yaml.Loader)['fit']
data_config = config['data']

# data_config['voxels_params']['r']['range'][1] = 100

dataset = RoadSegmentationDataset('val', 1, data_config['features'], data_config['voxels_params'],
                                  data_config['label_params'], data_config['time_concat'],
                                  data_config['num_points_set_abstraction'] * 5, cartesian_loss=False)
engine = dataset.engines[-1]

upsample_res_all = [(100, 100), (100, 100)]
r_bins = dataset.grid_bins['r']
az_bins = dataset.grid_bins['Az']

def grid_to_df(grid):
    pred_df = []

    for cls_i, upsample_res in zip([1, 2], upsample_res_all):
        pred = (grid == cls_i).astype(float)
        pred_r_grid, pred_az_grid = np.where(cv2.resize(pred, upsample_res) > 0.5)
        pred_r, pred_az = np.linspace(r_bins[0], r_bins[-1], upsample_res[0])[pred_r_grid], \
                          np.linspace(az_bins[0], az_bins[-1], upsample_res[1])[pred_az_grid]
        pred_x = pred_r * np.sin(np.deg2rad(pred_az))
        pred_y = pred_r * np.cos(np.deg2rad(pred_az))

        pred_df.append(pd.DataFrame({
            'x': pred_x, 'y': pred_y, 'az': pred_az, 'r': pred_r, 'cls_i': cls_i
        }))
    pred_df = pd.concat(pred_df)
    return pred_df


def soft_grid_to_df(grid, min_value=0, upsample_res=(300, 300)):
    resized = cv2.resize(grid, upsample_res)
    pred_r_grid, pred_az_grid = np.where(resized > min_value)
    pred_r, pred_az = np.linspace(r_bins[0], r_bins[-1], upsample_res[0])[pred_r_grid], \
                      np.linspace(az_bins[0], az_bins[-1], upsample_res[1])[pred_az_grid]
    pred_x = pred_r * np.sin(np.deg2rad(pred_az))
    pred_y = pred_r * np.cos(np.deg2rad(pred_az))
    pred_confidence = resized[pred_r_grid, pred_az_grid]

    pred_df = pd.DataFrame({
        'x': pred_x, 'y': pred_y, 'az': pred_az, 'r': pred_r, 'conf': pred_confidence
    })
    return pred_df

device = 'cpu'

model.to(device)
save_path = base_path / 'results' / experiment / f'epoch_{epoch_num}' / 'raw_predictions' / engine.get_drive_name()
os.makedirs(save_path, exist_ok=True)

def gen_prediction(idx):
    # sample = dataset.get_sample(engine, idx)
    model_input = dataset.get_model_input(engine, idx)
    if model_input is None:
        return None
    # model_input = sample['model_input']
    model_input['grouped_points'] = model_input['grouped_points'].unsqueeze(0).double().to(device)
    model_input['voxel_indices'] = model_input['voxel_indices'].unsqueeze(0)
    model_input['grid_shape'] = torch.Tensor(model_input['grid_shape']).unsqueeze(0).int()
    model_input['num_of_groups'] = torch.Tensor([model_input['num_of_groups']]).int()

    model_output = model(model_input)
    pred_multiclass_raw = model_output['prediction'].squeeze().detach().cpu().numpy()
    # label = sample['label']['segmentation'].numpy()
    np.save(save_path / f'{idx:05}', pred_multiclass_raw)
    # return pred_multiclass_raw
    # sample = {
    #     # "label": label,
    #     "prediction": pred_multiclass_raw,
    #     'grid_bins': dataset.grid_bins
    # }
    # return sample


pool = Pool(4)
with torch.inference_mode():
    # gen_prediction(0)
    # for idx_start in tqdm(range(5000, len(engine), 2000)):
        # for idx in tqdm(range(idx_start, idx_start + 500, 5)):
        #     # sample = gen_prediction(engine, idx)
        #     # pickle_write(save_path / f'{idx:05}.p', sample)
        #      gen_prediction(engine, idx)
    indices = range(1000, len(engine)+1, 300)
    # indices = sum([list(range(idx, idx+200, 10)) for idx in range(1000, len(engine)-500, 2000)], [])
    # indices = range(len(engine))
    list(tqdm(pool.imap(gen_prediction, indices), total=len(indices)))

pool.close()
