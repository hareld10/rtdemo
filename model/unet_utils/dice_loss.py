import torch


def dice_loss_multiclass(pred, target, smooth=1.):
    pred = pred.contiguous()
    target = target.contiguous()

    intersection = (pred * target).sum(dim=2).sum(dim=2)

    loss = (1 - ((2. * intersection + smooth) / (pred.sum(dim=2).sum(dim=2) + target.sum(dim=2).sum(dim=2) + smooth)))

    return loss.mean()


def dice_loss(pred_probs, label, weights=None, smooth=1.):
    intersection = torch.abs(pred_probs * label)
    mask_sum = label ** 2 + pred_probs ** 2

    # Soft Dice
    dice_loss = 1 - (2 * intersection + smooth) / (mask_sum + smooth)
    if weights is not None:
        dice_loss *= weights
    dice_loss = dice_loss.mean()
    return dice_loss
