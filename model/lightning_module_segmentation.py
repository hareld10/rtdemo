import pytorch_lightning as pl
import torch.optim
from einops import rearrange
from torch import nn

from unet_utils.unet_parts import UNet, OutConv


class BevRoadSegNet(pl.LightningModule):
    def __init__(self, num_of_features, input_encoder: torch.nn.Module, num_of_classes=3, regression_heads: list = None):
        super().__init__()
        self.save_hyperparameters()

        self.input_encoder = input_encoder
        self.unet = UNet(num_of_features, bilinear=False)

        self.segmentation_head = OutConv(self.unet.edge_dim, num_of_classes)
        self.regression_heads = nn.ModuleDict()
        if regression_heads is not None and len(regression_heads) > 0:
            assert not isinstance(regression_heads, str)
            for head_name in regression_heads:
                self.regression_heads[head_name] = OutConv(self.unet.edge_dim, 1)


    def forward(self, x):
        x = self.input_encoder(x)
        x = self.unet(x)

        raw_prediction = self.segmentation_head(x)
        segmentation_prediction = torch.sigmoid(raw_prediction)
        pred_dict = {'prediction': segmentation_prediction, 'raw_prediction': raw_prediction}
        if len(self.regression_heads) > 0:
            pred_dict['regression'] = {name: head(x).squeeze(1) for name, head in self.regression_heads.items()}

        return pred_dict

class Point2VoxelEncoder(nn.Module):

    def __init__(self, input_dims, encoding_dims, splits):
        super().__init__()
        self.splits = splits
        self.layers_split = nn.ModuleDict()
        self.encoding_dims = encoding_dims
        for split in splits:
            layers = []
            prev_dim = input_dims[split]
            for current_dim in encoding_dims:
                layers.append(nn.Conv1d(prev_dim, current_dim, 1))
                layers.append(nn.LeakyReLU())
                prev_dim = current_dim
            layers = nn.Sequential(*layers)

            self.layers_split[split] = layers

    def forward(self, model_input_all):
        '''
        input:
            points: [voxel, points_in_voxel, features]
            voxel_indices: [voxel, points_in_voxel]

        output:
            voxel grid
        '''
        existing_key = list(model_input_all.keys())[0]
        existing_input = model_input_all[existing_key]
        voxel_grid_shape = existing_input['grid_shape'][0]
        batch_size = max([len(model_input['num_of_groups']) for model_input in model_input_all.values()])
        dtype = existing_input['grouped_points'].dtype
        device = existing_input['grouped_points'].device
        voxel_grids_all = []
        for split in self.splits:
            voxel_grid = torch.zeros([batch_size, *voxel_grid_shape.cpu().numpy(), self.encoding_dims[-1]], dtype=dtype).to(
                device)

            if split in model_input_all:
                model_input = model_input_all[split]
                batch_pointer = torch.hstack(
                    [torch.ones(size, dtype=torch.int64) * i for i, size in enumerate(model_input['num_of_groups'])])
                points = model_input['grouped_points'].squeeze(0).permute(0, 2, 1)
                voxel_indices = model_input['voxel_indices'].squeeze(0).type(torch.int64)

                x = self.layers_split[split](points)
                x = torch.max(x, dim=2)[0]

                # voxel_grid[list(zip(*voxel_indices))] = x
                voxel_grid[batch_pointer, voxel_indices[:, 0], voxel_indices[:, 1]] = x
            voxel_grids_all.append(voxel_grid)
        voxel_grids_all = torch.concat(voxel_grids_all, dim=-1)
        return rearrange(voxel_grids_all, 'b h w c -> b c h w')
