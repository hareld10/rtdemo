import pathlib
import sys
import traceback
from itertools import product
from time import time

import numpy as np
import yaml

from model.semantic_wrapper import pred_to_boxes, SemanticWrapperVehicles

sys.path.insert(0, "/home/wisense/workspace/ai_infrastructure/")
sys.path.insert(0, "/home/wisense/workspace/rtdemo/model/")

import matplotlib.pyplot as plt
from pathlib import Path
from data_loaders.road_seg_data_loader import RoadSegmentationDataset, collate_grouped_point_clouds
from lightning_module_segmentation import BevRoadSegNet
import torch
import torch.multiprocessing as mp
import cv2
import pandas as pd

mp.set_start_method('spawn', force=True)
import matplotlib

plt.style.use('dark_background')
plt_font = 12

# change within gpu / cpu
# device = 'cpu'
device = 'cuda:0'

font = {'size': plt_font}
matplotlib.rc('font', **font)

module_path = str(pathlib.Path(__file__).parent.absolute())

base_path = Path(f"{module_path}/../")
# experiment = 'full_freq_pcp_multiclass_full_range/21-10-07-16:54:14'
# experiment = 'full_freq_pcp_multiclass_full_range/21-10-07-16:54:14'
experiments_dict = {
    'freespace': 'freespace_pcp_100m_with_height/21-12-31-12:30:46',
    'vehicle': 'vehicle_pcp/21-12-29-16:55:27'
}

models_dict = {}
datasets_dict = {}
tasks = ['freespace', 'vehicle']

for task in tasks:
    experiment = experiments_dict[task]
    resources_path = base_path / 'resources' / experiment.replace('/', '__')
    best_cp_path = resources_path / 'model_checkpoints.ckpt'
    config_path = resources_path / 'config.yaml'
    with open(config_path, 'r') as f:
        config = yaml.load(f)
    if 'fit' in config:
        config = config['fit']
    data_config = config['data']

    data_config.pop('num_workers'), data_config.pop('batch_size'), data_config.pop('validation_size')
    data_config['cartesian_loss'] = True
    data_config['num_points_set_abstraction'] *= 2

    datasets_dict[task] = RoadSegmentationDataset('val', **data_config)
    # r_bins = dataset.grid_bins['r']
    # az_bins = dataset.grid_bins['Az']
    # engine = [e for e in dataset.engines if '200831_lidar_drive_2_W92_M15_RF1' in e.get_drive_name()][0]
    models_dict[task] = BevRoadSegNet.load_from_checkpoint(best_cp_path).float()
    models_dict[task].to(device)

wrapper_vehicle = SemanticWrapperVehicles(datasets_dict['vehicle'], models_dict['vehicle'], 'cuda:0')


def gen_predictions(pcp_dict, ai_config):
    wrapper_vehicle.wrapper_params = ai_config['wrapper_params']
    pred_output = {}
    pcp_raw = pcp_dict['data']
    with torch.inference_mode():
        try:
            pcp_raw['Delta_Power'] = pcp_raw['delta_power']
            pcp_raw['FFT_Power'] = pcp_raw['fft_power']
            pcp_raw['r'] = np.sqrt(pcp_raw.x ** 2 + pcp_raw.y ** 2)
            pcp_raw['Az'] = np.rad2deg(np.arcsin(pcp_raw.x / pcp_raw.r))
            pcp_raw['El'] = np.rad2deg(np.arcsin(pcp_raw.z / pcp_raw.r))
            pcp_raw['delta_power_linear'] = np.power(10, pcp_raw['Delta_Power'] / 10)
            pcp_raw['fft_power_linear'] = np.power(10, pcp_raw['FFT_Power'] / 10)
            pcp_raw['noise_linear'] = np.power(10, pcp_raw['noise'] / 10)
            pcp_raw['vel'] = np.sqrt(pcp_raw.vx ** 2 + pcp_raw.vy ** 2 + pcp_raw.vz ** 2)
            pcp_raw = pcp_raw[(pcp_raw.r < 75) & (pcp_raw.FFT_Power > 0.5)]
            preds_dict = {}
            for task in ['freespace']:  # tasks:
                # t0 = time()
                dataset = datasets_dict[task]

                pcp = dataset.filter_cloud(pcp_raw)
                model_input = dataset.delta_to_model_input(pcp)
                model_input = collate_grouped_point_clouds([model_input])

                for split in model_input.keys():
                    model_input[split]['grouped_points'] = model_input[split]['grouped_points'].float().to(device)
                    model_input[split]['voxel_indices'] = model_input[split]['voxel_indices'].to(device)
                    model_input[split]['num_of_groups'] = model_input[split]['num_of_groups'].int().to(device)
                    model_input[split]['grid_shape'] = model_input[split]['grid_shape'].int().to(device)

            pred_3d = []
            if ai_config['flags']['vehicle'] or ai_config['flags']['vru']:
                # vehicle_conf = ai_config['vehicle']
                dynamic_boxes, static_boxes, pcp_dict_out_vehicle, metadata_vehicle = wrapper_vehicle.step(pcp_dict,
                                                                                                           model_input)
                pred_3d = dynamic_boxes + static_boxes
                print(pred_3d)
                pred_output['bboxs'] = []
                if ai_config['flags']['vehicle']:
                    vehicles_list = [
                        {'cx': x.tx, 'cy': x.ty, 'cz': 7, 'length': 4, 'width': 2, 'height': 1.5, 'heading': x.heading,
                         'cls': x.cls}
                        for x in pred_3d if x.cls == 'car']
                    pred_output['bboxs'].extend(vehicles_list)
                if ai_config['flags']['vru']:
                    ped_list = [
                        {'cx': x.tx, 'cy': x.ty, 'cz': 7, 'length': 1, 'width': 1, 'height': 2, 'heading': x.heading,
                         'cls': x.cls}
                        for x in pred_3d if x.cls == 'pedestrian']
                    pred_output['bboxs'].extend(ped_list)

            if ai_config['flags']['freespace']:
                fs_conf = ai_config['freespace']
                pred_fs = models_dict['freespace'](model_input)[
                    'prediction'].squeeze().cpu().numpy()

                # Perception filter
                if fs_conf['perception_filter']['radar_detections']:
                    for split in model_input.keys():
                        vi = model_input[split]['voxel_indices'].cpu()[0].numpy()
                        if vi.shape[0] > 0:
                            pred_fs[vi[:, 0], vi[:, 1]] = 0

                free_space_df = soft_grid_to_cartesian_scatter(pred_fs, datasets_dict['freespace'],
                                                1.5, 3, fs_conf['conf_thresh'])
                if fs_conf['perception_filter']['object_detections']:
                    free_space_mask = np.ones(free_space_df.shape[0], dtype=np.bool)
                    for obj in pred_3d:
                        free_space_mask &= ~obj.get_close_points(free_space_df, mask_only=True)
                    free_space_df = free_space_df[free_space_mask]

                free_space_df['z'] = -2
                pred_output['freespace'] = free_space_df
            return pred_output

        except KeyboardInterrupt as e:
            raise e

        except Exception as e:
            print(traceback.format_exc())
            return {}


upsample_res_all = [(100, 100), (100, 100)]


def soft_grid_to_df(grid, dataset, min_value=0, upsample_res=(300, 300)):
    r_bins = dataset.grid_bins['r']
    az_bins = dataset.grid_bins['Az']

    resized = cv2.resize(grid, upsample_res)
    pred_r_grid, pred_az_grid = np.where(resized > min_value)
    pred_r, pred_az = np.linspace(r_bins[0], r_bins[-1], upsample_res[0])[pred_r_grid], \
                      np.linspace(az_bins[0], az_bins[-1], upsample_res[1])[pred_az_grid]
    pred_x = pred_r * np.sin(np.deg2rad(pred_az))
    pred_y = pred_r * np.cos(np.deg2rad(pred_az))
    pred_confidence = resized[pred_r_grid, pred_az_grid]

    pred_df = pd.DataFrame({
        'x': pred_x, 'y': pred_y, 'az': pred_az, 'r': pred_r, 'conf': pred_confidence
    })
    return pred_df

def soft_grid_to_cartesian_scatter(grid, dataset, dx, dy, min_value=0):
    xlim, ylim = dataset.grid_df_xy[['x', 'y']].agg(['min', 'max']).values.T

    scatter_df = pd.DataFrame(list(product(np.arange(*xlim, dx), np.arange(*ylim, dy))), columns=['x', 'y'])
    scatter_df['r'] = np.sqrt(scatter_df.x ** 2 + scatter_df.y ** 2)
    scatter_df['Az'] = np.rad2deg(np.arcsin(scatter_df.x / scatter_df.r))
    scatter_df = dataset.filter_cloud(scatter_df)
    scatter_df = dataset.add_indices_to_cloud(scatter_df)[0]
    scatter_df['conf'] = grid[scatter_df.r_indices, scatter_df.Az_indices]
    scatter_df = scatter_df[scatter_df.conf >= min_value]
    return scatter_df
